package pl.silesiakursy.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.silesiakursy.demo.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
}
