package pl.silesiakursy.demo.converter;

import pl.silesiakursy.demo.dto.UserDTO;
import pl.silesiakursy.demo.entity.User;

public class UserToUserDTOConverter {

    public static UserDTO convert(User user) {

        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        return userDTO;
    }


}
