package pl.silesiakursy.demo.converter;

import pl.silesiakursy.demo.dto.UserDTO;
import pl.silesiakursy.demo.entity.User;

public class UserDTOToUserConverter {

    public static User convert(UserDTO userDTO)
    {
        User user = new User();
        user.setId(user.getId());
        user.setEmail(user.getEmail());
        user.setFirstName(user.getFirstName());
        user.setLastName(user.getLastName());
        return user;
    }
}
